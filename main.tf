provider  "aws"{
  region = local.region
  #access_key = "Enter AccessKey Here"
  #secret_key = "Enter Secret Key Here"
}


locals {
  db_identifier_prefix      = "${var.service_name}-postgres"
  final_snapshot_identifier = "${local.db_identifier_prefix}-final-snapshot"
  region = "us-east-1"

  # Change default values for read replica instance
  #is_read_replica = var.replicate_source_db == "" ? false : true
  #username        = local.is_read_replica ? "" : var.username
  #password        = local.is_read_replica ? "" : (var.snapshot_identifier == "" ? random_id.password.hex : "")

  username        = var.username
  password        = random_id.password.hex

  backup_retention_period = var.backup_retention_period
  skip_final_snapshot     = var.skip_final_snapshot
  copy_tags_to_snapshot   = var.copy_tags_to_snapshot
}

resource "random_id" "password" {
  byte_length = 8
}

#
# RDS DB Instance
resource "aws_db_instance" "postgres" {
  # Ignore changes on username as it is expected to be changed outside of Terraform or when restoring from snapshot
  # Ignore changes on password as it is expected to be changed outside of Terraform
  # Ignore changes on snapshot_identifier as it is expected to use this one time only when restoring from snapshot
  lifecycle {
    ignore_changes = [
      username,
      password,
      snapshot_identifier
    ]
  }

  count      = var.instance_count
  identifier = "${local.db_identifier_prefix}-${count.index}"

  # Indicates that this instance is a read replica
  #replicate_source_db = var.replicate_source_db

  engine         = "postgres"
  engine_version = var.engine_version
  instance_class = var.instance_class
  username       = local.username
  password       = local.password
  port           = var.port

  allocated_storage = var.allocated_storage
  storage_type      = var.storage_type
  iops              = var.iops
  storage_encrypted = var.storage_encrypted
  kms_key_id        = var.kms_key_id

  vpc_security_group_ids = var.vpc_security_group_ids
  multi_az               = var.multi_az
  publicly_accessible    = var.publicly_accessible

  db_subnet_group_name = var.db_subnet_group_name
  parameter_group_name = var.parameter_group_name

  allow_major_version_upgrade = var.allow_major_version_upgrade
  auto_minor_version_upgrade  = var.auto_minor_version_upgrade
  apply_immediately           = var.apply_immediately
  maintenance_window          = var.maintenance_window

  backup_retention_period = local.backup_retention_period
  backup_window           = var.backup_window

  skip_final_snapshot       = local.skip_final_snapshot
  final_snapshot_identifier = local.final_snapshot_identifier
  copy_tags_to_snapshot     = local.copy_tags_to_snapshot
  snapshot_identifier       = var.snapshot_identifier

  monitoring_interval = var.monitoring_interval
  monitoring_role_arn = var.enhanced_monitoring_arn

  tags = {
    Name          = "${local.db_identifier_prefix}-${count.index}"
    Service       = var.service_name
    ProductDomain = var.product_domain
    Environment   = var.environment
    Description   = var.description
    ManagedBy     = "Terraform"
  }
}

#
# CloudWatch resources
